package io.zhijian.app.router.bjq;

import io.zhijian.base.router.BaseRouter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * jiayy
 */
@Controller
@RequestMapping(value = "/admin/customer")
public class CustomerRouter extends BaseRouter{

    @Override
    protected String getPrefix() {
        return "/admin/bjq/customer";
    }
}
