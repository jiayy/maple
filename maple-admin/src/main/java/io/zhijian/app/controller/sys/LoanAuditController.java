package io.zhijian.app.controller.sys;

import com.baomidou.mybatisplus.plugins.Page;
import io.zhijian.base.controller.BaseController;
import io.zhijian.base.model.vo.DataGrid;
import io.zhijian.system.model.request.LoanAuditRequest;
import io.zhijian.system.model.response.LoanDetailResponse;
import io.zhijian.system.model.response.LoanListResponse;
import io.zhijian.system.model.response.UserResponse;
import io.zhijian.system.service.ILoanAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author JiaYY
 * @create 2017/7/24 22:15
 */
@RestController
@RequestMapping(value = "/loan/audit")
public class LoanAuditController extends BaseController {

    @Autowired
    private ILoanAuditService loanAuditService;

    /**
     * 查询分页
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public DataGrid getPage(LoanAuditRequest request) {
        Page<LoanListResponse> page = loanAuditService.getPage(getPagination(request), request);
        return super.buildDataGrid(page);
    }

    /**
     * 查询单个
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public LoanDetailResponse get(@PathVariable("id") Long loanId) {
        return loanAuditService.getLoanDetail(loanId);
    }

    /**
     * 借款审核通过
     */
    @RequestMapping(value = "/pass", method = RequestMethod.POST)
    public boolean pass(long loanId) {
        loanAuditService.auditLoan(loanId, 1);
        return true;
    }

    /**
     * 借款审核拒绝
     */
    @RequestMapping(value = "/refuse", method = RequestMethod.POST)
    public boolean refuse(long loanId) {
        loanAuditService.auditLoan(loanId, 2);
        return true;
    }
}
