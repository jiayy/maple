package io.zhijian.app.controller.sys;

import com.baomidou.mybatisplus.plugins.Page;
import io.zhijian.base.controller.BaseController;
import io.zhijian.base.model.vo.DataGrid;
import io.zhijian.system.model.request.CustomerRequest;
import io.zhijian.system.model.response.CustomerResponse;
import io.zhijian.system.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jiayy
 * @create 2017-07-16
 */
@RestController
@RequestMapping(value = "/loan/customer")
public class CustomerController extends BaseController {

    @Autowired
    private ICustomerService customerService;

    /**
     * 查询分页
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public DataGrid getPage(CustomerRequest request) {
        Page<CustomerResponse> page = customerService.getPage(getPagination(request), request);
        return super.buildDataGrid(page);
    }

}
