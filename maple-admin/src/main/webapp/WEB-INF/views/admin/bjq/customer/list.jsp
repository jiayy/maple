<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglibs.jsp" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <title>客户管理</title>

    <%@ include file="/WEB-INF/views/include/common.jsp" %>

</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false,minWidth:560">
    <div data-options="region:'north',border:false" style="padding: 10px 5px;">
        <input id="mobile" class="easyui-textbox" data-options="label:'手机号'" style="width:180px;"/>
        <input id="name" class="easyui-textbox" data-options="label:'姓名'" style="width:180px;"/>
        <a href="javascript:void(0)" onclick="queryUsers()" class="easyui-linkbutton button-blue"
           style="width: 70px;margin-left: 10px;">查&nbsp;询</a>
    </div>

    <div data-options="region:'center',border:false" style="height:100%">
        <table id="dg" style="width:100%;height:100%;">
        </table>
    </div>
</div>
<div id="dlg"></div>
<script>
    var datagrid;
    var dialog;
    $(function () {
        datagrid = $('#dg').datagrid({
            method: "get",
            url: '${ctx}/loan/customer/page',
            fit: true,
            fitColumns: true,
            border: true,
            idField: 'id',
            striped: true,
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 20,
            pageList: [10, 20, 30, 50, 100],
            singleSelect: true,
            selectOnCheck: true,
            checkOnSelect: true,
            toolbar: '#tb',
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {field: 'mobile', title: '手机号', sortable: false, width: 100},
                {field: 'name', title: '姓名', sortable: false, width: 100},
                {field: 'idCardNo', title: '身份证号', sortable: false, width: 160},
                {field: 'birthday', title: '出生日期', sortable: false},
                {
                    field: 'sex', title: '性别', sortable: false,
                    formatter: function (value, row, index) {
                        if(value == 1) {
                            return '男';
                        } else if(value == 2) {
                            return '女';
                        } else {
                            return '';
                        }
                    }
                },
                {
                    field: 'marryStatus', title: '婚姻状况', sortable: false,
                    formatter: function (value, row, index) {
                        if(value == 1) {
                            return '未婚';
                        } else if(value == 2) {
                            return '已婚';
                        } else {
                            return '';
                        }
                    }
                },
                {field: 'userLevel', title: '用户级别', sortable: false},
                {
                    field: 'isIdent', title: '实名认证', sortable: false,
                    formatter: function (value, row, index) {
                        return value == 1 ? '已认证' : '未认证';
                    }
                },
                {
                    field: 'isFace', title: '人脸认证', sortable: false,
                    formatter: function (value, row, index) {
                        return value == 1 ? '已认证' : '未认证';
                    }
                },
                {field: 'createTime', title: '注册时间', sortable: false, width: 150}
            ]],
            onLoadSuccess: function (data) {
                $('.button-delete').linkbutton({});
                $('.button-edit').linkbutton({});

                if (data) {
                    $.each(data.rows,
                        function (index, item) {
                            if (item.checked) {
                                $('#dg').datagrid('checkRow', index);
                            }
                        });
                }
            },
            onSelect: function (index, row) {
                if (row.isFixed == 1) {//固定的
                } else {
                }
            },
            queryParams: {
                name: encodeURI($('#name').val()),
                mobile: $('#mobile').val()
            }
        });
    });

    function queryUsers() {
        $(datagrid).datagrid('load', {
                name: encodeURI($('#name').val()),
                mobile: $('#mobile').val()
            }
        );
    }

</script>
</body>
</html>