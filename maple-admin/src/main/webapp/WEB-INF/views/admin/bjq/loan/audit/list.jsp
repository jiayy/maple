<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglibs.jsp" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <title>借款审核</title>

    <%@ include file="/WEB-INF/views/include/common.jsp" %>

</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false,minWidth:560">
    <div data-options="region:'north',border:false" style="padding: 10px 5px;">
        <input id="name" class="easyui-textbox" data-options="label:'姓名'" style="width:180px;"/>
        <input id="mobile" class="easyui-textbox" data-options="label:'手机号'" style="width:180px;"/>
        <a href="javascript:void(0)" onclick="queryUsers()" class="easyui-linkbutton button-blue"
           style="width: 70px;margin-left: 10px;">查&nbsp;询</a>
    </div>

    <div data-options="region:'center',border:false" style="height:100%">
        <table id="dg" style="width:100%;height:100%;">
        </table>
    </div>
</div>
<div id="dlg"></div>
<script>
    var datagrid;
    var dialog;
    $(function () {
        datagrid = $('#dg').datagrid({
            method: "get",
            url: '${ctx}/loan/audit/page',
            fit: true,
            fitColumns: true,
            border: true,
            idField: 'id',
            striped: true,
            pagination: true,
            rownumbers: true,
            pageNumber: 1,
            pageSize: 20,
            pageList: [10, 20, 30, 50, 100],
            singleSelect: true,
            selectOnCheck: true,
            checkOnSelect: true,
            toolbar: '#tb',
            columns: [[
                {field: 'id', title: 'id', hidden: true},
                {field: 'name', title: '姓名', sortable: false, width: 100},
                {field: 'mobile', title: '手机号', sortable: false, width: 100},
                {field: 'idCardNo', title: '身份证号', sortable: false, width: 160},
                {field: 'orderId', title: '订单编号', sortable: false},
                {
                    field: 'loanAmount', title: '借款金额', sortable: false,
                    formatter: function (value, row, index) {
                        return value/100;
                    }
                },
                {field: 'loanDays', title: '借款天数', sortable: false},
                {
                    field: 'loanStatus', title: '借款状态', sortable: false,
                    formatter: function (value, row, index) {
                        if(value == 0) {
                            return '待审核';
                        } else if(value == 1) {
                            return '通过';
                        } else if(value == 2) {
                            return '拒绝';
                        } else if(value == 3) {
                            return '回退';
                        } else {
                            return '';
                        }
                    }
                },
                {field: 'applyTime', title: '申请时间', sortable: false, width: 150},
                {
                    field: 'operate', title: '操作', width: 80,
                    formatter: function (value, row, index) {
                        var e = "<a onclick='audit(" + row.id + ")' class='button-edit button-blue'>开始审核</a>";
                        if (row.loanStatus == 0) {//固定的
                            return e;
                        } else {
                            return '';
                        }
                    }
                }
            ]],
            onLoadSuccess: function (data) {
                $('.button-delete').linkbutton({});
                $('.button-edit').linkbutton({});

                if (data) {
                    $.each(data.rows,
                        function (index, item) {
                            if (item.checked) {
                                $('#dg').datagrid('checkRow', index);
                            }
                        });
                }
            },
            onSelect: function (index, row) {
                if (row.isFixed == 1) {//固定的
                } else {
                }
            },
            queryParams: {
                name: encodeURI($('#name').val()),
                mobile: $('#mobile').val()
            }
        });
    });

    function queryUsers() {
        $(datagrid).datagrid('load', {
                name: encodeURI($('#name').val()),
                mobile: $('#mobile').val()
            }
        );
    }

    function audit(id) {
        if (id == null) {
            var row = datagrid.datagrid('getSelected');
            if (row == null) {
                U.msg('请先选择借款记录');
                return
            } else {
                id = row.id;
            }
        }

        dialog = $("#dlg").dialog({
            title: '借款审核',
            width: 880,
            height: 560,
            href: '${ctx}/admin/loan/audit/detail/' + id,
            maximizable: false,
            modal: true,
            buttons: [{
                text: '通过',
                iconCls: 'icon-ok',
                handler: function () {
                    U.post({
                        url: "${ctx}/loan/audit/pass",
                        loading: true,
                        data: {loanId:id},
                        success: function (data) {
                            if (data.code == 200) {
                                U.msg('操作成功');
                                dialog.dialog('close');
                                queryUsers();
                            } else if (data.code == 400) {//参数验证失败
                                U.msg('参数验证失败');
                            } else if (data.code == 404) {
                                U.msg('未找到该用户');
                            } else {
                                U.msg('服务器异常');
                            }
                        }
                    });
                }
            }, {
                text: '拒绝',
                iconCls: 'icon-no',
                handler: function () {
                    U.post({
                        url: "${ctx}/loan/audit/refuse",
                        loading: true,
                        data: {loanId:id},
                        success: function (data) {
                            if (data.code == 200) {
                                U.msg('操作成功');
                                dialog.dialog('close');
                                queryUsers();
                            } else if (data.code == 400) {//参数验证失败
                                U.msg('参数验证失败');
                            } else if (data.code == 404) {
                                U.msg('未找到该用户');
                            } else {
                                U.msg('服务器异常');
                            }
                        }
                    });
                }
            }, {
                text: '取消',
                handler: function () {
                    dialog.dialog('close');
                }
            }]
        });
    }

</script>
</body>
</html>