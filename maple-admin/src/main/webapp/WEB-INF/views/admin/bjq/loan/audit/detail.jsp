<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglibs.jsp" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <title>用户管理</title>

    <%@ include file="/WEB-INF/views/include/common.jsp" %>

</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'west',border:false" style="width:40%;">
        <form id="form">
            <table style="margin: 0 auto; padding: 10px; " cellspacing="0" cellpadding="0">
                <tr bgcolor="#d3d3d3">
                    <td colspan="2" align="center"><b>借款信息</b></td>
                </tr>
                <tr>
                    <td>借款金额：</td>
                    <td><input name="loanAmount" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>借款天数：</td>
                    <td><input name="loanDays" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>申请时间：</td>
                    <td><input name="applyTime" class="easyui-text"/></td>
                </tr>
                <tr bgcolor="#d3d3d3">
                    <td colspan="2" align="center"><b>个人信息</b></td>
                </tr>
                <tr>
                    <td>姓名：</td>
                    <td><input name="name" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>手机号：</td>
                    <td><input name="mobile" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>身份证号：</td>
                    <td><input name="idCardNo" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>出生年月：</td>
                    <td><input name="birthday" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>性别：</td>
                    <td><input name="sexName" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>婚姻状况：</td>
                    <td><input name="marryStatusName" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>芝麻信用：</td>
                    <td><input name="sesameScore" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>手机品牌：</td>
                    <td><input name="mobileBrand" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>手机型号：</td>
                    <td><input name="mobileModel" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>公司名称：</td>
                    <td><input name="companyName" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>职业类型：</td>
                    <td><input name="jobType" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>公司地址：</td>
                    <td><input name="companyDetailAddress" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>公司电话：</td>
                    <td><input name="companyPhone" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>部门职位：</td>
                    <td><input name="jobPosition" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>居住地址：</td>
                    <td><input name="homeDetailAddress" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>学历：</td>
                    <td><input name="eduGradeName" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>学校名称：</td>
                    <td><input name="schoolName" class="easyui-text"/></td>
                </tr>
                <tr>
                    <td>毕业状态：</td>
                    <td><input name="eduStatusName" class="easyui-text"/></td>
                </tr>
            </table>
        </form>
    </div>
    <div data-options="region:'center',border:false">
        <div data-options="region:'north',border:false" style="height:46%">
            <div style="text-align:center; height: 30px; line-height:30px;"><h5>身份证照片</h5></div>
            <div id="idCardDiv" style="text-align:center"></div>
        </div>
        <div class="datagrid-toolbar"></div>
        <div data-options="region:'center',border:false">
            <div style="text-align:center; height: 30px; line-height:30px;"><h5>人脸照片</h5></div>
            <div id="faceDiv" style="text-align:center"></div>
        </div>
    </div>
</div>
<style scoped>
    #form tr {
        line-height: 35px;
    }

    #form tr input, select {
        width: 220px;
    }
    img{
        margin:5px;
    }
</style>
<script>
    $(function () {
        U.get({
            url: "${ctx}/loan/audit/" + ${id},
            loading: true,
            success: function (data, status) {
                if (data.code == 200) {
                    $('#form').form('load', data.body);

                    if (data.body.idCardImgs) {
                        var idCardImgHtml = '';
                        var idCardImgs = data.body.idCardImgs;
                        for (var i = 0; i < idCardImgs.length; i++) {
                            idCardImgHtml += '&nbsp;<img src="' + idCardImgs[i] + '" width="220px" height="180px">&nbsp;';
                        }
                        $('#idCardDiv').html(idCardImgHtml);
                    }
                    if (data.body.faceImgs) {
                        var faceImgHtml = '';
                        var faceImgs = data.body.faceImgs;
                        for (var i = 0; i < faceImgs.length; i++) {
                            faceImgHtml += '&nbsp;<img src="' + faceImgs[i] + '" width="160px" height="200px">&nbsp;';
                        }
                        $('#faceDiv').html(faceImgHtml);
                    }

//                    $('#idCardDiv').html('<img src="http://pic.58pic.com/58pic/14/26/61/19k58PICmdI_1024.jpg" width="220px" height="180px">&nbsp;<img src="http://pic.58pic.com/58pic/14/26/61/19k58PICmdI_1024.jpg" width="220px" height="180px">');
//                    $('#faceDiv').html('<img src="http://pic.58pic.com/58pic/14/26/61/19k58PICmdI_1024.jpg" width="160px" height="200px">&nbsp;<img src="http://pic.58pic.com/58pic/14/26/61/19k58PICmdI_1024.jpg" width="160px" height="200px">&nbsp;<img src="http://pic.58pic.com/58pic/14/26/61/19k58PICmdI_1024.jpg" width="160px" height="200px">');
                } else if (data.code == 400) {//参数验证失败
                    U.msg('参数验证失败');
                } else if (data.code == 404) {
                    U.msg('未找到该用户');
                } else {
                    U.msg('服务器异常');
                }
            }
        });
    });
</script>
</body>
</html>