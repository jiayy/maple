<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglibs.jsp" %>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <title>后台管理界面平台</title>

    <%@ include file="/WEB-INF/views/include/common.jsp" %>

    <script src="${ctxstatic}/plugin/Highcharts-5.0.0/js/highcharts.js"></script>
    <script src="${ctxstatic}/plugin/justgage-1.2.2/raphael-2.1.4.min.js"></script>
    <script src="${ctxstatic}/plugin/justgage-1.2.2/justgage.js"></script>

</head>

<body style="padding: 20px">
<div class="easyui-layout" data-options="fit:true">
    <div data-options="region:'center',border:false" style="height:40%">
        <div class="theme-user-info-panel">
            <div class="left">
                <img src="${ctxstatic}/plugin/easyui/themes/insdep/images/portrait86x86.png" width="86" height="86" onerror="javascript:this.src='${ctxstatic}/plugin/easyui/themes/insdep/images/portrait86x86.png'">
            </div>
            <div class="right">


            </div>
            <div class="center">
                <h1>${user.name}<%--匿名--%></h1>
                <h2>${user.username}</h2>
                <dl>
                    <dt>${user.email}<%--examples@insdep.com--%></dt>
                    <dd>${user.mobile}<%--13000000000--%></dd>
                </dl>
            </div>

        </div>
    </div>
</div>
</div>
</body>
</html>