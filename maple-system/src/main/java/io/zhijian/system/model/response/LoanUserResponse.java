package io.zhijian.system.model.response;

/**
 * @author JiaYY
 * @create 2017/7/25 0:16
 */
public class LoanUserResponse {

    private String userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 身份证号
     */
    private String idCardNo;

    /**
     * 身份证签发机关
     */
    private String idCardOffice;

    /**
     * 身份证有效期
     */
    private String idCardTerm;

    /**
     * 出生日期
     */
    private String birthday;

    /**
     * 性别，0未知，1男，2女
     */
    private Integer sex;

    /**
     * 是否实名认证，0未认证，1已认证
     */
    private Integer isIdent;

    /**
     * 是否扫脸认证，0未认证，1已认证
     */
    private Integer isFace;

    /**
     * 是否帮卡，0未绑卡，1已绑卡
     */
    private Integer isBind;

    /**
     * 职业类型
     */
    private String jobType;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 公司区域id
     */
    private Integer companyRegionId;

    /**
     * 公司详细地址
     */
    private String companyDetailAddress;

    /**
     * 公司电话
     */
    private String companyPhone;

    /**
     * 工作职位
     */
    private String jobPosition;

    /**
     * 职业信息认证状态，0未提交，1待认证，2已认证
     */
    private Integer jobIdentStatus;

    /**
     * 居住地址区域id
     */
    private Integer homeRegionId;

    /**
     * 居住详细地址
     */
    private String homeDetailAddress;

    /**
     * 婚姻状况，0未知，1未婚，2已婚
     */
    private Integer marryStatus;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 个人信息认证状态，0未提交，1待认证，2已认证
     */
    private Integer personIdentStatus;

    /**
     * 学历，0未提交
     */
    private Integer eduGrade;

    /**
     * 毕业状态，0未提交，1未毕业，2已毕业
     */
    private Integer eduStatus;

    /**
     * 学校区域id
     */
    private Integer schoolRegionId;

    /**
     * 学校名称
     */
    private String schoolName;

    /**
     * 学校信息认证状态，0未提交，1待认证，2已认证
     */
    private Integer schoolIdentStatus;

    /**
     * 亲友信息认证状态，0未提交，1待认证，2已认证
     */
    private Integer friendIdentStatus;

    /**
     * 芝麻信用授权状态，0未授权，1已授权
     */
    private Integer sesameStatus;

    /**
     * 芝麻信用分
     */
    private Integer sesameScore;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getIdCardOffice() {
        return idCardOffice;
    }

    public void setIdCardOffice(String idCardOffice) {
        this.idCardOffice = idCardOffice;
    }

    public String getIdCardTerm() {
        return idCardTerm;
    }

    public void setIdCardTerm(String idCardTerm) {
        this.idCardTerm = idCardTerm;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getIsIdent() {
        return isIdent;
    }

    public void setIsIdent(Integer isIdent) {
        this.isIdent = isIdent;
    }

    public Integer getIsFace() {
        return isFace;
    }

    public void setIsFace(Integer isFace) {
        this.isFace = isFace;
    }

    public Integer getIsBind() {
        return isBind;
    }

    public void setIsBind(Integer isBind) {
        this.isBind = isBind;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyRegionId() {
        return companyRegionId;
    }

    public void setCompanyRegionId(Integer companyRegionId) {
        this.companyRegionId = companyRegionId;
    }

    public String getCompanyDetailAddress() {
        return companyDetailAddress;
    }

    public void setCompanyDetailAddress(String companyDetailAddress) {
        this.companyDetailAddress = companyDetailAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public Integer getJobIdentStatus() {
        return jobIdentStatus;
    }

    public void setJobIdentStatus(Integer jobIdentStatus) {
        this.jobIdentStatus = jobIdentStatus;
    }

    public Integer getHomeRegionId() {
        return homeRegionId;
    }

    public void setHomeRegionId(Integer homeRegionId) {
        this.homeRegionId = homeRegionId;
    }

    public String getHomeDetailAddress() {
        return homeDetailAddress;
    }

    public void setHomeDetailAddress(String homeDetailAddress) {
        this.homeDetailAddress = homeDetailAddress;
    }

    public Integer getMarryStatus() {
        return marryStatus;
    }

    public void setMarryStatus(Integer marryStatus) {
        this.marryStatus = marryStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPersonIdentStatus() {
        return personIdentStatus;
    }

    public void setPersonIdentStatus(Integer personIdentStatus) {
        this.personIdentStatus = personIdentStatus;
    }

    public Integer getEduGrade() {
        return eduGrade;
    }

    public void setEduGrade(Integer eduGrade) {
        this.eduGrade = eduGrade;
    }

    public Integer getEduStatus() {
        return eduStatus;
    }

    public void setEduStatus(Integer eduStatus) {
        this.eduStatus = eduStatus;
    }

    public Integer getSchoolRegionId() {
        return schoolRegionId;
    }

    public void setSchoolRegionId(Integer schoolRegionId) {
        this.schoolRegionId = schoolRegionId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Integer getSchoolIdentStatus() {
        return schoolIdentStatus;
    }

    public void setSchoolIdentStatus(Integer schoolIdentStatus) {
        this.schoolIdentStatus = schoolIdentStatus;
    }

    public Integer getFriendIdentStatus() {
        return friendIdentStatus;
    }

    public void setFriendIdentStatus(Integer friendIdentStatus) {
        this.friendIdentStatus = friendIdentStatus;
    }

    public Integer getSesameStatus() {
        return sesameStatus;
    }

    public void setSesameStatus(Integer sesameStatus) {
        this.sesameStatus = sesameStatus;
    }

    public Integer getSesameScore() {
        return sesameScore;
    }

    public void setSesameScore(Integer sesameScore) {
        this.sesameScore = sesameScore;
    }
}
