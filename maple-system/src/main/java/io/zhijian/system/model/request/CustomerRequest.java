package io.zhijian.system.model.request;

import io.zhijian.base.model.request.BaseRequest;

/**
 * @author JiaYY
 * @create 2017/7/16 23:51
 */
public class CustomerRequest extends BaseRequest {

    /*** 姓名     */
    private String name;

    /*** 手机号     */
    private String mobile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
