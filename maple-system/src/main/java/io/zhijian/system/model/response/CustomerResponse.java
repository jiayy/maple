package io.zhijian.system.model.response;

import io.zhijian.base.model.response.BaseResponse;

/**
 * @author JiaYY
 * @create 2017/7/16 23:57
 */
public class CustomerResponse extends BaseResponse {

    /*** 姓名     */
    private String name;

    /*** 手机号     */
    private String mobile;

    /**
     * 身份证号
     */
    private String idCardNo;

    /**
     * 出生日期
     */
    private String birthday;

    /**
     * 性别，0未知，1男，2女
     */
    private Integer sex;

    /**
     * 婚姻状况，0未知，1未婚，2已婚
     */
    private Integer marryStatus;

    /**
     * 用户级别
     */
    private Integer userLevel;

    /**
     * 是否实名认证，0未认证，1已认证
     */
    private Integer isIdent;

    /**
     * 是否扫脸认证，0未认证，1已认证
     */
    private Integer isFace;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getMarryStatus() {
        return marryStatus;
    }

    public void setMarryStatus(Integer marryStatus) {
        this.marryStatus = marryStatus;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public Integer getIsIdent() {
        return isIdent;
    }

    public void setIsIdent(Integer isIdent) {
        this.isIdent = isIdent;
    }

    public Integer getIsFace() {
        return isFace;
    }

    public void setIsFace(Integer isFace) {
        this.isFace = isFace;
    }
}
