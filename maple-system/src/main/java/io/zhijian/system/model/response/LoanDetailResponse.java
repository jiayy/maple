package io.zhijian.system.model.response;

import io.zhijian.base.model.response.BaseResponse;

import java.util.Date;
import java.util.List;

/**
 * @author JiaYY
 * @create 2017/7/16 23:57
 */
public class LoanDetailResponse extends BaseResponse {

    private String userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 身份证号
     */
    private String idCardNo;

    /**
     * 身份证签发机关
     */
    private String idCardOffice;

    /**
     * 身份证有效期
     */
    private String idCardTerm;

    /**
     * 出生日期
     */
    private String birthday;

    /**
     * 性别，0未知，1男，2女
     */
    private String sexName;

    /**
     * 手机品牌
     */
    private String mobileBrand;

    /**
     * 手机型号
     */
    private String mobileModel;

    /**
     * 职业类型
     */
    private String jobType;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 公司详细地址
     */
    private String companyDetailAddress;

    /**
     * 公司电话
     */
    private String companyPhone;

    /**
     * 工作职位
     */
    private String jobPosition;

    /**
     * 居住详细地址
     */
    private String homeDetailAddress;

    /**
     * 婚姻状况，0未知，1未婚，2已婚
     */
    private String marryStatusName;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 学历，0未提交
     */
    private String eduGradeName;

    /**
     * 毕业状态，0未提交，1未毕业，2已毕业
     */
    private String eduStatusName;

    /**
     * 学校名称
     */
    private String schoolName;

    /**
     * 芝麻信用分
     */
    private Integer sesameScore;

    /**
     * 借款金额，单位分
     */
    private Long loanAmount;

    /**
     * 借款天数
     */
    private Integer loanDays;

    private String applyTime;

    private List<String> idCardImgs;

    private List<String> faceImgs;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getIdCardOffice() {
        return idCardOffice;
    }

    public void setIdCardOffice(String idCardOffice) {
        this.idCardOffice = idCardOffice;
    }

    public String getIdCardTerm() {
        return idCardTerm;
    }

    public void setIdCardTerm(String idCardTerm) {
        this.idCardTerm = idCardTerm;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSexName() {
        return sexName;
    }

    public void setSexName(String sexName) {
        this.sexName = sexName;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyDetailAddress() {
        return companyDetailAddress;
    }

    public void setCompanyDetailAddress(String companyDetailAddress) {
        this.companyDetailAddress = companyDetailAddress;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getHomeDetailAddress() {
        return homeDetailAddress;
    }

    public void setHomeDetailAddress(String homeDetailAddress) {
        this.homeDetailAddress = homeDetailAddress;
    }

    public String getMarryStatusName() {
        return marryStatusName;
    }

    public void setMarryStatusName(String marryStatusName) {
        this.marryStatusName = marryStatusName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEduGradeName() {
        return eduGradeName;
    }

    public void setEduGradeName(String eduGradeName) {
        this.eduGradeName = eduGradeName;
    }

    public String getEduStatusName() {
        return eduStatusName;
    }

    public void setEduStatusName(String eduStatusName) {
        this.eduStatusName = eduStatusName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Integer getSesameScore() {
        return sesameScore;
    }

    public void setSesameScore(Integer sesameScore) {
        this.sesameScore = sesameScore;
    }

    public String getMobileBrand() {
        return mobileBrand;
    }

    public void setMobileBrand(String mobileBrand) {
        this.mobileBrand = mobileBrand;
    }

    public String getMobileModel() {
        return mobileModel;
    }

    public void setMobileModel(String mobileModel) {
        this.mobileModel = mobileModel;
    }

    public Long getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getLoanDays() {
        return loanDays;
    }

    public void setLoanDays(Integer loanDays) {
        this.loanDays = loanDays;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public List<String> getIdCardImgs() {
        return idCardImgs;
    }

    public void setIdCardImgs(List<String> idCardImgs) {
        this.idCardImgs = idCardImgs;
    }

    public List<String> getFaceImgs() {
        return faceImgs;
    }

    public void setFaceImgs(List<String> faceImgs) {
        this.faceImgs = faceImgs;
    }
}
