package io.zhijian.system.model.response;

import io.zhijian.base.model.response.BaseResponse;

import java.util.Date;

/**
 * @author JiaYY
 * @create 2017/7/16 23:57
 */
public class LoanListResponse extends BaseResponse {

    private String userId;

    /*** 姓名     */
    private String name;

    /*** 手机号     */
    private String mobile;

    /**
     * 身份证号
     */
    private String idCardNo;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 借款金额，单位分
     */
    private Long loanAmount;

    /**
     * 借款天数
     */
    private Integer loanDays;

    /**
     * 申请时间
     */
    private Date applyTime;

    /**
     * 借款状态，0待审核，1通过，2拒绝，3回退
     */
    private Integer loanStatus;

    /**
     * 放款时间
     */
    private Date loanTime;

    /**
     * 收款账户
     */
    private String receiveAccount;

    /**
     * 到账时间
     */
    private Date receiveTime;

    /**
     * 服务费用，单位分
     */
    private Long serviceCharge;

    /**
     * 合约还款日
     */
    private Date contractRepayDate;

    /**
     * 实际还款日
     */
    private Date realRepayDate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getLoanDays() {
        return loanDays;
    }

    public void setLoanDays(Integer loanDays) {
        this.loanDays = loanDays;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Integer getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(Integer loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Date getLoanTime() {
        return loanTime;
    }

    public void setLoanTime(Date loanTime) {
        this.loanTime = loanTime;
    }

    public String getReceiveAccount() {
        return receiveAccount;
    }

    public void setReceiveAccount(String receiveAccount) {
        this.receiveAccount = receiveAccount;
    }

    public Date getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Long getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(Long serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public Date getContractRepayDate() {
        return contractRepayDate;
    }

    public void setContractRepayDate(Date contractRepayDate) {
        this.contractRepayDate = contractRepayDate;
    }

    public Date getRealRepayDate() {
        return realRepayDate;
    }

    public void setRealRepayDate(Date realRepayDate) {
        this.realRepayDate = realRepayDate;
    }
}
