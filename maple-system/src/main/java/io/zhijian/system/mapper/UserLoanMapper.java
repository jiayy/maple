package io.zhijian.system.mapper;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import io.zhijian.base.mapper.BaseMapper;
import io.zhijian.system.entity.UserInfo;
import io.zhijian.system.entity.UserLoan;
import io.zhijian.system.model.request.CustomerRequest;
import io.zhijian.system.model.request.LoanAuditRequest;
import io.zhijian.system.model.response.LoanListResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author JiaYY
 * @create 2017/7/17 0:03
 */
public interface UserLoanMapper extends BaseMapper<UserLoan> {

    List<LoanListResponse> findUserLoan(Pagination page, @Param("request") LoanAuditRequest request);

    void updateLoanStatus(@Param("id") long loanId, @Param("loanStatus") int loanStatus);
}
