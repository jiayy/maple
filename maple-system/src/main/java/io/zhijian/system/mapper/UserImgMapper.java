package io.zhijian.system.mapper;

import io.zhijian.system.entity.UserImg;

import java.util.List;

/**
 * @author JiaYY
 * @create 2017/7/27 23:24
 */
public interface UserImgMapper {

    List<UserImg> findUserImgList(String userId);
}
