package io.zhijian.system.mapper;

import io.zhijian.system.entity.UserExtend;

/**
 * @author JiaYY
 * @create 2017/7/25 23:33
 */
public interface UserExtendMapper {

    UserExtend findUserExtend(String userId);
}
