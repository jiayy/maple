package io.zhijian.system.mapper;

import io.zhijian.system.entity.SysRegion;

/**
 * @author JiaYY
 * @create 2017/7/26 0:07
 */
public interface SysRegionMapper {

    SysRegion findRegionById(int id);
}
