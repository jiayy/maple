package io.zhijian.system.mapper;

import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import io.zhijian.base.mapper.BaseMapper;
import io.zhijian.system.entity.UserInfo;
import io.zhijian.system.model.request.CustomerRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author JiaYY
 * @create 2017/7/17 0:03
 */
public interface CustomerMapper extends BaseMapper<UserInfo> {

    List<UserInfo> findCustomer(Pagination page, @Param("request") CustomerRequest request);

    UserInfo findByUserId(String userId);
}
