package io.zhijian.system.mapper;

import io.zhijian.system.entity.UserLogin;

/**
 * @author JiaYY
 * @create 2017/7/25 23:57
 */
public interface UserLoginMapper {

    UserLogin findUserLogin(String userId);
}
