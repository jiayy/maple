package io.zhijian.system.enums;

/**
 * 枚举
 *
 */
public enum SexType {

    DEFAULT(0, ""),
    MAN(1, "男"),
    WOMAN(2, "女");

    private int code;
    private String name;

    SexType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getCodeName(int code) {
        for (SexType cs : SexType.values()) {
            if (cs.getCode() == code) {
                return cs.getName();
            }
        }
        return "";
    }
}
