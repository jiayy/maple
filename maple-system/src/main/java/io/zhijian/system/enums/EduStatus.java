package io.zhijian.system.enums;

/**
 * 枚举
 *
 */
public enum EduStatus {

    DEFAULT(0, ""),
    NO(1, "未毕业"),
    YES(2, "已毕业");

    private int code;
    private String name;

    EduStatus(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getCodeName(int code) {
        for (EduStatus cs : EduStatus.values()) {
            if (cs.getCode() == code) {
                return cs.getName();
            }
        }
        return "";
    }
}
