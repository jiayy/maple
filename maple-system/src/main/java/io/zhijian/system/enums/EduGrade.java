package io.zhijian.system.enums;

/**
 * 枚举
 *
 */
public enum EduGrade {
    DEFAULT(0, ""),
    ZHONGZHUAN(1, "中专、高中及以下"),
    DAZHUAN(2, "大专"),
    BENKE(3, "本科"),
    SHUOSHI(4, "硕士"),
    BOSHI(5, "博士");

    private int code;
    private String name;

    EduGrade(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getCodeName(int code) {
        for (EduGrade cs : EduGrade.values()) {
            if (cs.getCode() == code) {
                return cs.getName();
            }
        }
        return "";
    }
}
