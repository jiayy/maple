package io.zhijian.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import io.zhijian.system.entity.UserInfo;
import io.zhijian.system.model.request.CustomerRequest;
import io.zhijian.system.model.response.CustomerResponse;

/**
 * @author JiaYY
 * @create 2017/7/16 23:55
 */
public interface ICustomerService {

    Page<CustomerResponse> getPage(Page<UserInfo> page, CustomerRequest request);

    UserInfo findByUserId(String userId);
}
