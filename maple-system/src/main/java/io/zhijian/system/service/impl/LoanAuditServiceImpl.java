package io.zhijian.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import io.zhijian.base.service.impl.BaseService;
import io.zhijian.system.entity.*;
import io.zhijian.system.enums.EduGrade;
import io.zhijian.system.enums.EduStatus;
import io.zhijian.system.enums.MarryStatus;
import io.zhijian.system.enums.SexType;
import io.zhijian.system.mapper.*;
import io.zhijian.system.model.request.LoanAuditRequest;
import io.zhijian.system.model.response.LoanDetailResponse;
import io.zhijian.system.model.response.LoanListResponse;
import io.zhijian.system.service.ICustomerService;
import io.zhijian.system.service.ILoanAuditService;
import io.zhijian.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author JiaYY
 * @create 2017/7/16 23:58
 */
@Service
public class LoanAuditServiceImpl extends BaseService<UserLoanMapper, UserLoan> implements ILoanAuditService {

    private static final String IMG_URL = "http://47.93.14.98/image";

    @Autowired
    private ICustomerService customerService;

    @Autowired
    private UserLoginMapper userLoginMapper;

    @Autowired
    private UserExtendMapper userExtendMapper;

    @Autowired
    private SysRegionMapper sysRegionMapper;

    @Autowired
    private UserImgMapper userImgMapper;

    @Override
    public Page<LoanListResponse> getPage(Page<LoanListResponse> page, LoanAuditRequest request) {
        if(request.getName() != null && !"".equals(request.getName())) {
            try {
                request.setName(URLDecoder.decode(request.getName(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        List<LoanListResponse> userLoans = baseMapper.findUserLoan(page, request);
        page.setRecords(userLoans);
        return convert(page, LoanListResponse.class);
    }

    @Override
    public LoanDetailResponse getLoanDetail(long loanId) {
        //借款信息
        LoanDetailResponse response = new LoanDetailResponse();
        UserLoan userLoan = baseMapper.selectById(loanId);
        if(userLoan == null) {
            return response;
        }
        response.setLoanAmount(userLoan.getLoanAmount()/100);
        response.setLoanDays(userLoan.getLoanDays());
        response.setApplyTime(DateUtils.format(userLoan.getApplyTime(), "yyyy-MM-dd HH:mm:ss"));

        String userId = userLoan.getUserId();
        UserInfo userInfo = customerService.findByUserId(userId);
        if(userInfo == null) {
            return response;
        }
        BeanUtils.copyProperties(userInfo, response);
        response.setSexName(SexType.getCodeName(userInfo.getSex()));
        response.setMarryStatusName(MarryStatus.getCodeName(userInfo.getMarryStatus()));
        //登录信息
        UserLogin userLogin = userLoginMapper.findUserLogin(userId);
        if(userLogin != null) {
            response.setMobileBrand(userLogin.getMobileBrand());
            response.setMobileModel(userLogin.getMobileModel());
        }
        //扩展信息
        UserExtend userExtend = userExtendMapper.findUserExtend(userId);
        if(userExtend != null) {
            BeanUtils.copyProperties(userExtend, response);
            response.setEduGradeName(EduGrade.getCodeName(userExtend.getEduGrade()));
            response.setEduStatusName(EduStatus.getCodeName(userExtend.getEduStatus()));
            response.setCompanyDetailAddress(userExtend.getCompanyDetailAddress());
            response.setHomeDetailAddress(userExtend.getHomeDetailAddress());

            if(userExtend.getCompanyRegionId() > 0) {
                SysRegion sysRegion = sysRegionMapper.findRegionById(userExtend.getCompanyRegionId());
                if(sysRegion != null) {
                    response.setCompanyDetailAddress(sysRegion.getFullName() + userExtend.getCompanyDetailAddress());
                }
            }
            if(userExtend.getHomeRegionId() > 0) {
                SysRegion sysRegion = sysRegionMapper.findRegionById(userExtend.getHomeRegionId());
                if(sysRegion != null) {
                    response.setHomeDetailAddress(sysRegion.getFullName() + userExtend.getHomeDetailAddress());
                }
            }
        }
        //身份证和人脸照片
        List<UserImg> imgList = userImgMapper.findUserImgList(userId);
        if(imgList != null && imgList.size() > 0) {
            List<String> idCardImgs = new ArrayList<>();
            List<String> faceImgs = new ArrayList<>();
            boolean idCardFront = false;
            boolean idCardBack = false;
            for(UserImg userImg : imgList) {
                if(userImg.getImgType() == 1 && !idCardFront) {
                    idCardFront = true;
                    idCardImgs.add(IMG_URL + userImg.getImgUrl());
                } else if(userImg.getImgType() == 2 && !idCardBack) {
                    idCardBack = true;
                    idCardImgs.add(IMG_URL + userImg.getImgUrl());
                } else if(userImg.getImgType() == 3 && faceImgs.size() < 3) {
                    faceImgs.add(IMG_URL + userImg.getImgUrl());
                }
            }
            response.setIdCardImgs(idCardImgs);
            response.setFaceImgs(faceImgs);
        }
        return response;
    }

    @Override
    public void auditLoan(long loanId, int loanStatus) {
        baseMapper.updateLoanStatus(loanId, loanStatus);
    }
}
