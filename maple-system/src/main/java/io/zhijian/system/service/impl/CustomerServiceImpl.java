package io.zhijian.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import io.zhijian.base.service.impl.BaseService;
import io.zhijian.system.entity.UserInfo;
import io.zhijian.system.mapper.CustomerMapper;
import io.zhijian.system.model.request.CustomerRequest;
import io.zhijian.system.model.response.CustomerResponse;
import io.zhijian.system.service.ICustomerService;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * @author JiaYY
 * @create 2017/7/16 23:58
 */
@Service
public class CustomerServiceImpl extends BaseService<CustomerMapper, UserInfo> implements ICustomerService {

    @Override
    public Page<CustomerResponse> getPage(Page<UserInfo> page, CustomerRequest request) {
        if(request.getName() != null && !"".equals(request.getName())) {
            try {
                request.setName(URLDecoder.decode(request.getName(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        List<UserInfo> users = baseMapper.findCustomer(page, request);
        page.setRecords(users);
        return convert(page, CustomerResponse.class);
    }

    @Override
    public UserInfo findByUserId(String userId) {
        return baseMapper.findByUserId(userId);
    }
}
