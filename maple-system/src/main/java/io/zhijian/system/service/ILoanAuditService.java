package io.zhijian.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import io.zhijian.system.model.request.LoanAuditRequest;
import io.zhijian.system.model.response.LoanDetailResponse;
import io.zhijian.system.model.response.LoanListResponse;

/**
 * @author JiaYY
 * @create 2017/7/24 22:17
 */
public interface ILoanAuditService {

    Page<LoanListResponse> getPage(Page<LoanListResponse> page, LoanAuditRequest request);

    LoanDetailResponse getLoanDetail(long loanId);

    void auditLoan(long loanId, int loanStatus);
}
