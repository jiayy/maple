package io.zhijian.system.entity;

import java.util.Date;

public class UserInfo {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 身份证号
     */
    private String idCardNo;

    /**
     * 身份证签发机关
     */
    private String idCardOffice;

    /**
     * 身份证有效期
     */
    private String idCardTerm;

    /**
     * 出生日期
     */
    private String birthday;

    /**
     * 性别，0未知，1男，2女
     */
    private Integer sex;

    /**
     * 婚姻状况，0未知，1未婚，2已婚
     */
    private Integer marryStatus;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 用户级别
     */
    private Integer userLevel;

    /**
     * 是否实名认证，0未认证，1已认证
     */
    private Integer isIdent;

    /**
     * 是否扫脸认证，0未认证，1已认证
     */
    private Integer isFace;

    /**
     * 职业信息认证状态，0未提交，1已认证
     */
    private Integer jobIdentStatus;

    /**
     * 个人信息认证状态，0未提交，1已认证
     */
    private Integer personIdentStatus;

    /**
     * 亲友信息认证状态，0未提交，1已认证
     */
    private Integer friendIdentStatus;

    /**
     * 学校信息认证状态，0未提交，1已认证
     */
    private Integer schoolIdentStatus;

    /**
     * 芝麻信用授权状态，0未授权，1已授权
     */
    private Integer sesameStatus;

    /**
     * 芝麻信用用户id
     */
    private String sesameOpenId;

    /**
     * 芝麻信用分
     */
    private Integer sesameScore;

    /**
     * 锁定状态，0未锁定，1已锁定
     */
    private Integer lockStatus;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo == null ? null : idCardNo.trim();
    }

    public String getIdCardOffice() {
        return idCardOffice;
    }

    public void setIdCardOffice(String idCardOffice) {
        this.idCardOffice = idCardOffice == null ? null : idCardOffice.trim();
    }

    public String getIdCardTerm() {
        return idCardTerm;
    }

    public void setIdCardTerm(String idCardTerm) {
        this.idCardTerm = idCardTerm == null ? null : idCardTerm.trim();
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getMarryStatus() {
        return marryStatus;
    }

    public void setMarryStatus(Integer marryStatus) {
        this.marryStatus = marryStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public Integer getIsIdent() {
        return isIdent;
    }

    public void setIsIdent(Integer isIdent) {
        this.isIdent = isIdent;
    }

    public Integer getIsFace() {
        return isFace;
    }

    public void setIsFace(Integer isFace) {
        this.isFace = isFace;
    }

    public Integer getJobIdentStatus() {
        return jobIdentStatus;
    }

    public void setJobIdentStatus(Integer jobIdentStatus) {
        this.jobIdentStatus = jobIdentStatus;
    }

    public Integer getPersonIdentStatus() {
        return personIdentStatus;
    }

    public void setPersonIdentStatus(Integer personIdentStatus) {
        this.personIdentStatus = personIdentStatus;
    }

    public Integer getFriendIdentStatus() {
        return friendIdentStatus;
    }

    public void setFriendIdentStatus(Integer friendIdentStatus) {
        this.friendIdentStatus = friendIdentStatus;
    }

    public Integer getSchoolIdentStatus() {
        return schoolIdentStatus;
    }

    public void setSchoolIdentStatus(Integer schoolIdentStatus) {
        this.schoolIdentStatus = schoolIdentStatus;
    }

    public Integer getSesameStatus() {
        return sesameStatus;
    }

    public void setSesameStatus(Integer sesameStatus) {
        this.sesameStatus = sesameStatus;
    }

    public String getSesameOpenId() {
        return sesameOpenId;
    }

    public void setSesameOpenId(String sesameOpenId) {
        this.sesameOpenId = sesameOpenId == null ? null : sesameOpenId.trim();
    }

    public Integer getSesameScore() {
        return sesameScore;
    }

    public void setSesameScore(Integer sesameScore) {
        this.sesameScore = sesameScore;
    }

    public Integer getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Integer lockStatus) {
        this.lockStatus = lockStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}