package io.zhijian.system.entity;

import java.util.Date;

public class UserImg {
    /**
     * 
     */
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 图片类型，0头像，1身份证正面，2身份证背面，3人脸照片
     */
    private Integer imgType;

    /**
     * 图片名称
     */
    private String imgName;

    /**
     * 真实名称
     */
    private String realName;

    /**
     * 图片链接
     */
    private String imgUrl;

    /**
     * 图片大小
     */
    private Long imgSize;

    /**
     * 上传时间
     */
    private Date uploadTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getImgType() {
        return imgType;
    }

    public void setImgType(Integer imgType) {
        this.imgType = imgType;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName == null ? null : imgName.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    public Long getImgSize() {
        return imgSize;
    }

    public void setImgSize(Long imgSize) {
        this.imgSize = imgSize;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }
}