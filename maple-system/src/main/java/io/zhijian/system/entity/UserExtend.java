package io.zhijian.system.entity;

import java.util.Date;

public class UserExtend {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 职业类型
     */
    private String jobType;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 公司区域id
     */
    private Integer companyRegionId;

    /**
     * 公司详细地址
     */
    private String companyDetailAddress;

    /**
     * 公司电话
     */
    private String companyPhone;

    /**
     * 部门职位
     */
    private String jobPosition;

    /**
     * 职业信息提交时间
     */
    private Date jobSubmitTime;

    /**
     * 居住地址区域id
     */
    private Integer homeRegionId;

    /**
     * 居住详细地址
     */
    private String homeDetailAddress;

    /**
     * 个人信息提交时间
     */
    private Date personSubmitTime;

    /**
     * 学历，0未提交，1中专、高中及以下，2大专，3本科，4硕士，5博士
     */
    private Integer eduGrade;

    /**
     * 毕业状态，0未提交，1在读，2毕业
     */
    private Integer eduStatus;

    /**
     * 学校区域id
     */
    private Integer schoolRegionId;

    /**
     * 学校名称
     */
    private String schoolName;

    /**
     * 教育信息提交时间
     */
    private Date schoolSubmitTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType == null ? null : jobType.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public Integer getCompanyRegionId() {
        return companyRegionId;
    }

    public void setCompanyRegionId(Integer companyRegionId) {
        this.companyRegionId = companyRegionId;
    }

    public String getCompanyDetailAddress() {
        return companyDetailAddress;
    }

    public void setCompanyDetailAddress(String companyDetailAddress) {
        this.companyDetailAddress = companyDetailAddress == null ? null : companyDetailAddress.trim();
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone == null ? null : companyPhone.trim();
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition == null ? null : jobPosition.trim();
    }

    public Date getJobSubmitTime() {
        return jobSubmitTime;
    }

    public void setJobSubmitTime(Date jobSubmitTime) {
        this.jobSubmitTime = jobSubmitTime;
    }

    public Integer getHomeRegionId() {
        return homeRegionId;
    }

    public void setHomeRegionId(Integer homeRegionId) {
        this.homeRegionId = homeRegionId;
    }

    public String getHomeDetailAddress() {
        return homeDetailAddress;
    }

    public void setHomeDetailAddress(String homeDetailAddress) {
        this.homeDetailAddress = homeDetailAddress == null ? null : homeDetailAddress.trim();
    }

    public Date getPersonSubmitTime() {
        return personSubmitTime;
    }

    public void setPersonSubmitTime(Date personSubmitTime) {
        this.personSubmitTime = personSubmitTime;
    }

    public Integer getEduGrade() {
        return eduGrade;
    }

    public void setEduGrade(Integer eduGrade) {
        this.eduGrade = eduGrade;
    }

    public Integer getEduStatus() {
        return eduStatus;
    }

    public void setEduStatus(Integer eduStatus) {
        this.eduStatus = eduStatus;
    }

    public Integer getSchoolRegionId() {
        return schoolRegionId;
    }

    public void setSchoolRegionId(Integer schoolRegionId) {
        this.schoolRegionId = schoolRegionId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName == null ? null : schoolName.trim();
    }

    public Date getSchoolSubmitTime() {
        return schoolSubmitTime;
    }

    public void setSchoolSubmitTime(Date schoolSubmitTime) {
        this.schoolSubmitTime = schoolSubmitTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}