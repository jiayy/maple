package io.zhijian.system.entity;

import java.util.Date;

public class UserLoan {
    /**
     * 主键id
     */
    private Long id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 借款金额，单位分
     */
    private Long loanAmount;

    /**
     * 借款天数
     */
    private Integer loanDays;

    /**
     * 申请时间
     */
    private Date applyTime;

    /**
     * 借款状态，0待审核，1通过，2拒绝，3回退
     */
    private Integer loanStatus;

    /**
     * 放款时间
     */
    private Date loanTime;

    /**
     * 收款账户
     */
    private String receiveAccount;

    /**
     * 到账时间
     */
    private Date receiveTime;

    /**
     * 服务费用，单位分
     */
    private Long serviceCharge;

    /**
     * 合约还款日
     */
    private Date contractRepayDate;

    /**
     * 实际还款日
     */
    private Date realRepayDate;

    /**
     * 是否删除，0未删除，1已删除
     */
    private Integer isDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public Long getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getLoanDays() {
        return loanDays;
    }

    public void setLoanDays(Integer loanDays) {
        this.loanDays = loanDays;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Integer getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(Integer loanStatus) {
        this.loanStatus = loanStatus;
    }

    public Date getLoanTime() {
        return loanTime;
    }

    public void setLoanTime(Date loanTime) {
        this.loanTime = loanTime;
    }

    public String getReceiveAccount() {
        return receiveAccount;
    }

    public void setReceiveAccount(String receiveAccount) {
        this.receiveAccount = receiveAccount == null ? null : receiveAccount.trim();
    }

    public Date getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Long getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(Long serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public Date getContractRepayDate() {
        return contractRepayDate;
    }

    public void setContractRepayDate(Date contractRepayDate) {
        this.contractRepayDate = contractRepayDate;
    }

    public Date getRealRepayDate() {
        return realRepayDate;
    }

    public void setRealRepayDate(Date realRepayDate) {
        this.realRepayDate = realRepayDate;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}