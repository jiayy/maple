package io.zhijian.system.entity;

public class SysRegion {
    /**
     * 
     */
    private Integer id;

    /**
     * 行政区名称
     */
    private String name;

    /**
     * 父级地域id
     */
    private Integer fid;

    /**
     * 行政区级别
     */
    private Integer level;

    /**
     * 省级id
     */
    private Integer provinceId;

    /**
     * 市级id 0 表示是比市级更高的行政区，即省
     */
    private Integer cityId;

    /**
     * 县级id 0 表示是比县级更高的行政区
     */
    private Integer countyId;

    /**
     * 行政区全名
     */
    private String fullName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName == null ? null : fullName.trim();
    }
}